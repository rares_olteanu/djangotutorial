
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout

from .models import Choice, Question
from .forms import UserForm, QuestionForm, ChoicesForm


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):

        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):

        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()

        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


def login_user(request):
    form = UserForm(request.POST or None)
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                questions = Question.objects.filter(user=request.user)
                return render(request, 'polls/user_page.html', {'questions': questions, 'name': user.username})
            else:
                return render(request, 'polls/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'polls/login.html', {'error_message': 'Invalid login'})
    context = {
        "form": form,
    }
    return render(request, 'polls/login.html', context)


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                own_questions = Question.objects.filter(user=request.user)
                return render(request, 'polls/user_page.html', {'question': own_questions})
    context = {
        "form": form,
    }
    return render(request, 'polls/register.html', context)


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'polls/login.html', context)


def add_question(request):
    if not request.user.is_authenticated():
        return render(request, 'polls/add_question.html')
    else:
        form = QuestionForm(request.POST or None)
        if form.is_valid():
            question = form.save(commit=False)
            question.user = request.user
            question.question_text = question.question_text
            question.pub_date = timezone.now()
            question.save()
            my_questions=Question.objects.filter(user=request.user).order_by('-pub_date')

            return render(request, 'polls/user_page.html', {'questions': my_questions})
        context = {
            "form": form,
        }
        return render(request, 'polls/add_question.html', context)


def add_choice(request, question_id):
    form = ChoicesForm(request.POST or None)
    question = get_object_or_404(Question, pk=question_id)
    choices = question.choice_set.all()
    if form.is_valid():
        for s in choices:
            if s.choice_text == form.cleaned_data.get("choice_text"):
                context = {
                    'choices': choices,
                    'question': question,
                    'form': form,
                }
                return render(request, 'polls/add_choice.html', context)
        choice = form.save(commit=False)
        choice.question = question
        choice.choice_text = choice.choice_text
        choice.save()
        my_questions = Question.objects.filter(user=request.user).order_by('-pub_date')
        return render(request, 'polls/user_page.html', {'questions': my_questions})
    context = {
        'choices': choices,
        'question': question,
        'form': form,
    }
    return render(request, 'polls/add_choice.html', context)
