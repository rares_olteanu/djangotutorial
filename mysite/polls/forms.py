from django import forms
from django.contrib.auth.models import User

from .models import Question, Choice


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']


class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ['question_text']


class ChoicesForm(forms.ModelForm):

    class Meta:
        model = Choice
        fields = ['choice_text']


